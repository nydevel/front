export { getPosts, getPost, deletePost } from './api/Api';
export type { Post } from './model/Post';

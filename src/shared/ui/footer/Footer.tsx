import { Layout } from 'antd';

export const Footer = () => {
  return <Layout.Footer>SHIFU ©2023</Layout.Footer>;
};
